package com.java.demo.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Picture {

    private String name;
    private String tipo;
    private String fonte;
    private Long tamanho;


}



