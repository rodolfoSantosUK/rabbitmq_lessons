package com.java.demo.controller;

import com.java.demo.config.MessagingConfig;
import com.java.demo.dto.Pedido;
import com.java.demo.dto.StatusPedido;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
@RequestMapping("/rh")
public class RecursosHumanosController {

    @Autowired
    private RabbitTemplate template;

    @PostMapping("/{nomeDoRestaurante}")
    public ResponseEntity salvarPedido(@RequestBody Pedido pedido, @PathVariable  String nomeDoRestaurante) {
        pedido.setPedidoId(UUID.randomUUID().toString());
        StatusPedido statusPedido = new StatusPedido(pedido, "PROCESS", "criado pedido com sucesso" +  nomeDoRestaurante);
        template.convertAndSend(MessagingConfig.EXCHANGE_HR, "", statusPedido );
        return ResponseEntity.ok("Sucesso");
    }

}
