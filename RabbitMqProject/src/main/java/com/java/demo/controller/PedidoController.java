package com.java.demo.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.java.demo.config.MessagingConfig;
import com.java.demo.dto.Pedido;
import com.java.demo.dto.StatusPedido;
import lombok.var;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
@RequestMapping("/pedido")
public class PedidoController {

    @Autowired
    private RabbitTemplate template;

    @Autowired
    private ObjectMapper objectMapper;

    @PostMapping("/{nomeDoRestaurante}")
    public ResponseEntity salvarPedido(@RequestBody Pedido pedido, @PathVariable  String nomeDoRestaurante) throws JsonProcessingException {
        pedido.setPedidoId(UUID.randomUUID().toString());
        StatusPedido statusPedido = new StatusPedido(pedido, "PROCESS", "criado pedido com sucesso" +  nomeDoRestaurante);
        String jsonStatusPedido  = objectMapper.writeValueAsString(statusPedido);

        template.convertAndSend(MessagingConfig.EXCHANGE, MessagingConfig.ROUTING_KEY, jsonStatusPedido );
        return ResponseEntity.ok("Sucesso");
    }

}
