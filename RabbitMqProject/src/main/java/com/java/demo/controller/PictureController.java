package com.java.demo.controller;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.java.demo.config.MessagingConfig;
import com.java.demo.entity.Picture;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

@RestController
@RequestMapping("/picture")
public class PictureController {

    @Autowired
    private RabbitTemplate template;

    @Autowired
    private ObjectMapper objectMapper;

    private List<String> SOURCES = Arrays.asList("mobile", "web");
    private List<String> TIPOS = Arrays.asList("jpg", "png", "svg");


    @GetMapping()
    public ResponseEntity salvarPedido() throws JsonProcessingException {

        for(int i =0; i < 30; i++) {

            Picture _picture = new Picture();
            _picture.setName("Picture " + i);
            _picture.setTamanho(ThreadLocalRandom.current().nextLong(1, 10000));
            _picture.setFonte(SOURCES.get(i % SOURCES.size()));
            _picture.setTipo(TIPOS.get(i % TIPOS.size()));

            String json = objectMapper.writeValueAsString(_picture);
            template.convertAndSend(MessagingConfig.EXCHANGE_PICTURE, _picture.getTipo(), json );

        }
        return ResponseEntity.ok("Sucesso");
    }


}
