package com.java.demo.consumer;

import com.java.demo.config.MessagingConfig;
import com.java.demo.dto.StatusPedido;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

@Component
public class UsuarioConsumer {

    @RabbitListener(queues = MessagingConfig.QUEUE, concurrency = "3-7")
    public void consumeMessageFromQueue(StatusPedido statusPedido) {
        System.out.println("Mensagem recebida da fila : "  +  statusPedido);
    }

}
