package com.java.demo.consumer;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.java.demo.config.MessagingConfig;
import com.java.demo.dto.StatusPedido;
import com.java.demo.entity.Picture;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


@Component
public class PictureConsumer {

    private static final Logger LOG = LoggerFactory.getLogger(PictureConsumer.class);

    @Autowired
    private ObjectMapper objectMapper;



    @RabbitListener(queues = MessagingConfig.QUEUE_PICTURE_IMAGE)
    public void consumeMessageFromQueue(String message) throws JsonProcessingException {

        Picture picture = objectMapper.readValue(message, Picture.class);
        LOG.info("Valor de picture : {}", picture);

    }

    @RabbitListener(queues = MessagingConfig.QUEUE_PICTURE_VECTOR)
    public void consumeMessage(String message) throws JsonProcessingException {

        Picture picture = objectMapper.readValue(message, Picture.class);
        LOG.info("Valor de Vector : {}", picture);

    }

}
