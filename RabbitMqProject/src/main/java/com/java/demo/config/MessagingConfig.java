package com.java.demo.config;

import org.springframework.amqp.core.*;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


@Configuration
public class MessagingConfig {

    public static final String QUEUE = "q.javatechie";
    public static final String QUEUE_PICTURE_IMAGE = "q.picture.image";
    public static final String QUEUE_PICTURE_VECTOR = "q.picture.vector";
    public static final String EXCHANGE = "x.javatechie";
    public static final String EXCHANGE_HR = "x.hr";
    public static final String EXCHANGE_PICTURE = "x.picture";
    public static final String ROUTING_KEY = "javatechie";

    @Bean
    public Queue queue() {
        return new Queue(QUEUE);
    }

    @Bean
    public TopicExchange exchange() {
        return new TopicExchange(EXCHANGE);
    }

    @Bean
    public Binding binding(Queue queue, TopicExchange exchange ) {
        return BindingBuilder.bind(queue).to(exchange()).with(ROUTING_KEY);
    }

    @Bean
    public AmqpTemplate template(ConnectionFactory connectionFactory) {
        final RabbitTemplate rabbitTemplate = new RabbitTemplate(connectionFactory);
        return rabbitTemplate;
    }


}
