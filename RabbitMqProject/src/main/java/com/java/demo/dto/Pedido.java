package com.java.demo.dto;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Pedido {

    private String pedidoId;
    private String name;
    private int quantidade;
    private Double preco;

}
